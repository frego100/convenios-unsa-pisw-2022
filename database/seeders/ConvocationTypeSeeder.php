<?php

namespace Database\Seeders;

use App\Models\ConvocationType;
use Illuminate\Database\Seeder;

class ConvocationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConvocationType::create([
            'name'=>'Extraordinario - Estudiante',
            'acronym'=>'PIVE'
        ]);
        ConvocationType::create([
            'name'=>'Extraordinario - Docente',
            'acronym'=>'PIVDO'
        ]);
        ConvocationType::create([
            'name'=>'Ordinaria Docentes Vienen',
            'acronym'=>'CODVIENEN'
        ]);
        ConvocationType::create([
            'name'=>'Ordinaria Docentes Van',
            'acronym'=>'CODVAN'
        ]);
        ConvocationType::create([
            'name'=>'Ordinaria Estudiantes Vienen',
            'acronym'=>'COEVIENEN'
        ]);
        ConvocationType::create([
            'name'=>'Ordinaria Estudiantes Van',
            'acronym'=>'COEVAN'
        ]);
    }
}
