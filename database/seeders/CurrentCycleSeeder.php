<?php

namespace Database\Seeders;

use App\Models\CurrentCycle;
use Illuminate\Database\Seeder;

class CurrentCycleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CurrentCycle::create([
            'description'=>'1º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'2º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'3º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'4º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'5º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'6º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'7º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'8º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'9º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'10º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'11º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        CurrentCycle::create([
            'description'=>'12º ciclo',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
