<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name'=>'Admin',
            'lastname'=>'Admin',
            'address'=>'Admin address',
            'phone'=>'999999999',
            'email'=>'admin@unsa.edu.pe',
            'password'=>'admin',
            'role'=>'1',
            'log_user_created'=>'admin',
            'log_user_modified'=>'admin'
        ]);
        Admin::create([
            'name'=>'admin',
            'lastname'=>'admin',
            'address'=>'Admin address',
            'phone'=>'992999999',
            'email'=>'admin2@unsa.edu.pe',
            'password'=>'admin',
            'role'=>'2',
            'log_user_created'=>'admin',
            'log_user_modified'=>'admin'
        ]);
    }
}
