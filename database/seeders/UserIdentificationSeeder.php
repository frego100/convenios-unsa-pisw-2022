<?php

namespace Database\Seeders;

use App\Models\UserIdentification;
use Illuminate\Database\Seeder;

class UserIdentificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserIdentification::create([
            'value'=>'12345',
            'type'=>'1'
        ]);
    }
}
