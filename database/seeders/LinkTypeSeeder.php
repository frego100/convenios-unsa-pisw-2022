<?php

namespace Database\Seeders;

use App\Models\LinkType;
use Illuminate\Database\Seeder;

class LinkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        LinkType::create([
            'name'=>'Formulario Universidad de destino',
            'category'=>'Categoria de Link 1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        LinkType::create([
            'name'=>'Oferta academica',
            'category'=>'Categoria de Link 2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
