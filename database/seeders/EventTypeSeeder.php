<?php

namespace Database\Seeders;

use App\Models\EventType;
use Illuminate\Database\Seeder;

class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::create([
            'name'=>'Foro'
        ]);

        EventType::create([
            'name'=>'Simposio'
        ]);

        EventType::create([
            'name'=>'Congreso'
        ]);

        EventType::create([
            'name'=>'Seminarios'
        ]);

    }
}
