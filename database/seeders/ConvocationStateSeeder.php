<?php

namespace Database\Seeders;

use App\Models\ConvocationState;
use Illuminate\Database\Seeder;

class ConvocationStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConvocationState::create([
            'name'=>'Proxima',
            'description'=>'Esta es una convocatoria proxima a lanzar.'
        ]);
        ConvocationState::create([
            'name'=>'En proceso',
            'description'=>'Esta es una convocatoria en proceso.'
        ]);
        ConvocationState::create([
            'name'=>'En evaluacion',
            'description'=>'Esta es una convocatoria en evaluacion.'
        ]);
        ConvocationState::create([
            'name'=>'Cancelada',
            'description'=>'Esta es una convocatoria cancelada.'
        ]);
        ConvocationState::create([
            'name'=>'Re abierta',
            'description'=>'Esta es una convocatoria reabierta.'
        ]);
        ConvocationState::create([
            'name'=>'En seguimiento',
            'description'=>'Esta es una convocatoria en seguimiento.'
        ]);
        ConvocationState::create([
            'name'=>'En finalizacion',
            'description'=>'Esta es una convocatoria en finalizacion.'
        ]);
        ConvocationState::create([
            'name'=>'Concluida',
            'description'=>'Esta es una convocatoria concluida.'
        ]);
    }
}
