<?php

namespace Database\Seeders;

use App\Models\Program;
use Illuminate\Database\Seeder;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Program::create([
            'name'=>'Programa Profesional de Quimica',
            'acronym' => 'PPQ',
            'faculty' => '1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Ambiental',
            'acronym' => 'PPIA',
            'faculty' => '1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Materiales',
            'acronym' => 'PPIM',
            'faculty' => '1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Metalurgica',
            'acronym' => 'PPIM',
            'faculty' => '1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Industrias Alimentarias',
            'acronym' => 'PPIIA',
            'faculty' => '1',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Sistemas',
            'acronym' => 'PPIS',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Electrica',
            'acronym' => 'PPIE',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Electronica',
            'acronym' => 'PPIE',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Mecanica',
            'acronym' => 'PPIM',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Industrial',
            'acronym' => 'PPII',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ciencia de la Computacion',
            'acronym' => 'PPCC',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Telecomunicaciones',
            'acronym' => 'PPIT',
            'faculty' => '2',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Geofisica',
            'acronym' => 'PPIG',
            'faculty' => '3',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Geologica',
            'acronym' => 'PPIG',
            'faculty' => '3',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria de Minas',
            'acronym' => 'PPIM',
            'faculty' => '3',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Civil',
            'acronym' => 'PPIC',
            'faculty' => '4',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Sanitaria',
            'acronym' => 'PPIS',
            'faculty' => '4',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Fisica',
            'acronym' => 'PPF',
            'faculty' => '5',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Matematicas',
            'acronym' => 'PPM',
            'faculty' => '5',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Quimica',
            'acronym' => 'PPQ',
            'faculty' => '5',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Arquitectura',
            'acronym' => 'PPA',
            'faculty' => '6',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Biologia',
            'acronym' => 'PPB',
            'faculty' => '7',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ciencias de la Nutricion',
            'acronym' => 'PPCN',
            'faculty' => '7',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Ingenieria Pesquera',
            'acronym' => 'PPIP',
            'faculty' => '7',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Medicina',
            'acronym' => 'PPM',
            'faculty' => '8',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Enfermeria',
            'acronym' => 'PPM',
            'faculty' => '9',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Program::create([
            'name'=>'Programa Profesional de Agronomia',
            'acronym' => 'PPA',
            'faculty' => '10',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
