<?php

namespace Database\Seeders;

use App\Models\AdminRoles;
use Illuminate\Database\Seeder;

class AdminRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminRoles::create([
            'name'=>'Super administrador',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AdminRoles::create([
            'name'=>'Administrador',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
