<?php

namespace Database\Seeders;

use App\Models\PostulationCoevanState;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Storage::deleteDirectory('photoProfiles');
        $this->call(UserSeeder::class);
        $this->call(ProfileTypeSeeder::class);
        $this->call(DocumentTypeSeeder::class);
        $this->call(UserIdentificationTypeSeeder::class);
        $this->call(UserIdentificationSeeder::class);
        $this->call(ProfileSeeder::class);
        $this->call(ConvocationTypeSeeder::class);
        $this->call(EventTypeSeeder::class);
        //$this->call(ConvocationSeeder::class);
        $this->call(RequirementSeeder::class);
        $this->call(AdminRolesSeeder::class);
        $this->call(AdminSeeder::class);
        //$this->call(AcademicNetworkSeeder::class);
        $this->call(LinkTypeSeeder::class);
        //$this->call(UniversitySeeder::class);
        $this->call(FacultySeeder::class);
        $this->call(ProgramSeeder::class);
        $this->call(AcademicYearSeeder::class);
        $this->call(CurrentCycleSeeder::class);
        $this->call(ModalitySeeder::class);
        //$this->call(ExternalStudentSeeder::class);
        $this->call(ConvocationStateSeeder::class);
        $this->call(PostulationCoevanStateSeeder::class);
           
    }
}
