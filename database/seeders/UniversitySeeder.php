<?php

namespace Database\Seeders;

use App\Models\University;
use Illuminate\Database\Seeder;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        University::create([
            'name'=>'Universidad Nacional de San Agustin de Arequipa',
            'acronym' => 'UNSA',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
        
        University::create([
            'name'=>'Universidad Catolica San Pablo',
            'acronym' => 'UCSP',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        University::create([
            'name'=>'Universidad Catolica Santa Maria',
            'acronym' => 'UCSM',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        University::create([
            'name'=>'Universidad Peruana de Ciencias Aplicadas',
            'acronym' => 'UPC',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        University::create([
            'name'=>'Universidad Nacional de Ingenieria',
            'acronym' => 'UNI',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
