<?php

namespace Database\Seeders;

use App\Models\Requirement;
use Illuminate\Database\Seeder;

class RequirementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Requirement::create([
            'description'=>'No Haber participado en una movilidad PIVE'
        ]);

        Requirement::create([
            'description'=>'Pertenecer al tercio superior'
        ]);

        Requirement::create([
            'description'=>'Tener nivel de inglés avanzado'
        ]);
    }
}
