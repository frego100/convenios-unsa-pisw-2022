<?php

namespace Database\Seeders;

use App\Models\AcademicYear;
use Illuminate\Database\Seeder;

class AcademicYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicYear::create([
            'description'=>'1º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'2º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'3º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'4º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'5º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'6º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicYear::create([
            'description'=>'7º año',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
