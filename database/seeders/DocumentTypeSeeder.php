<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

use function PHPSTORM_META\map;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DocumentType::create([
            'description'=>'CONVOCATION DOCUMENT',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
        
        DocumentType::create([
            'description'=>'CONVOCATION AFICHE',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        DocumentType::create([
            'description'=>'CONVOCATION BANNER',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
