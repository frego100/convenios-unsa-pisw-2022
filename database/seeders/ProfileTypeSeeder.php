<?php

namespace Database\Seeders;

use App\Models\ProfileType;
use Illuminate\Database\Seeder;

class ProfileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfileType::create([
            'description'=>'Estudiante'
        ]);

        ProfileType::create([
            'description'=>'Docente'
        ]);
    }
}
