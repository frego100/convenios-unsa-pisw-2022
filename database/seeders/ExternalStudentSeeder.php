<?php

namespace Database\Seeders;

use App\Models\ExternalStudent;
use Illuminate\Database\Seeder;

class ExternalStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ExternalStudent::create([
            'name'=>'Ana Lucia',
            'lastname' => 'Angles Quiroz',
            'email'=>'test01@test.com',
            'justification' => 'justification',
            'status_request' => '1',
            'email_created' => 'created@unsa.edu.pe'
        ]);
        ExternalStudent::create([
            'name'=>'Maribel Araceli',
            'lastname' => 'Quispe Yncarroque',
            'email'=>'test02@test.com',
            'justification' => 'justification',
            'status_request' => '1',
            'email_created' => 'created@unsa.edu.pe'
        ]);
        ExternalStudent::create([
            'name'=>'Carlos Alberto',
            'lastname' => 'Mestas Escarcena',
            'email'=>'test03@test.com',
            'justification' => 'justification'
        ]);
        ExternalStudent::create([
            'name'=>'Jordan Uriel',
            'lastname' => 'Laqui Pacaya',
            'email'=>'test04@test.com',
            'justification' => 'justification'
        ]);
        ExternalStudent::create([
            'name'=>'Royer Michael',
            'lastname' => 'Quispe Ccoya',
            'email'=>'test05@test.com',
            'justification' => 'justification'
        ]);
    }
}
