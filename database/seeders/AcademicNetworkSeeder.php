<?php

namespace Database\Seeders;

use App\Models\AcademicNetwork;
use Illuminate\Database\Seeder;

class AcademicNetworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AcademicNetwork::create([
            'name'=>'Red Peruana de Universidades Nacionales para la Internacionalización',
            'acronym'=>'RUNAi',
            'description'=>'Descripcion de RUNAi',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        AcademicNetwork::create([
            'name'=>'Red Peruana de Universidades',
            'acronym'=>'RPU',
            'description'=>'Descripcion de RPU',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
