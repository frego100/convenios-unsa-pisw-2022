<?php

namespace Database\Seeders;

use App\Models\Modality;
use Illuminate\Database\Seeder;

class ModalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modality::create([
            'name'=>'Presencial',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Modality::create([
            'name'=>'Virtual',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
