<?php

namespace Database\Seeders;

use App\Models\UserIdentificationType;
use Illuminate\Database\Seeder;

class UserIdentificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserIdentificationType::create([
            'description'=>"D.N.I"
        ]);

        UserIdentificationType::create([
            'description'=>"Pasaporte"
        ]);

        UserIdentificationType::create([
            'description'=>"Cedula"
        ]);
    }
}
