<?php

namespace Database\Seeders;

use App\Models\PostulationCoevanState;
use Illuminate\Database\Seeder;

class PostulationCoevanStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostulationCoevanState::create([
            'name' => 'Sin enviar',
            'description' => 'El usuario postulante guardó la información en su formulario, pero no envió aún su postulación.'

        ]);

        PostulationCoevanState::create([
            'name' => 'Enviado',
            'description' => 'El usuario postulante envió su postulación.'

        ]);

        PostulationCoevanState::create([
            'name' => 'En revisión',
            'description' => 'La postulación del usuario está en revisión.'

        ]);

        PostulationCoevanState::create([
            'name' => 'Postulación Aceptado',
            'description' => 'La postulación ha sido aceptada con todos los datos conformes, una vez revisado si no hay observaciones, entonces el administrador puede darle a aceptar postulación.'

        ]);

        PostulationCoevanState::create([
            'name' => 'Observada (Con alguna observación)',
            'description' => 'Se ha encontrado una observación en los datos de postulación, para esto el administrador debe adjuntar un documento(“esquela”) breve de la observación.'

        ]);

        PostulationCoevanState::create([
            'name' => 'Postulación Ganada',
            'description' => 'El postulante asociado a la postulación es ganador, para pasar a este estado, se debe adjuntar por parte del administrador la carta de aceptación.'

        ]);
        PostulationCoevanState::create([
            'name' => 'En documentación',
            'description' => 'El postulante debe enviar los documentos que le soliciten para llevar a cabo su pasantía, dentro de eso la: carta de compromiso, para pasar a este estado; Solicitud de constancia de becario; Constancia de matrícula.'

        ]);
        PostulationCoevanState::create([
            'name' => 'En Seguimiento',
            'description' => 'Para pasar a este estado, el administrador debe colocar la fecha de inicio de clases, con ello se generarán los meses para el seguimiento  del. El estudiante ya inició su pasantía y está en etapa de seguimiento donde tiene que entregar sus reportes mensuales, solicitud de reconocimiento de notas.'

        ]);
        PostulationCoevanState::create([
            'name' => 'En seguimiento con Observación',
            'description' => 'La postulación tiene observaciones por diferentes razones, como por ejemplo, el estudiante no ha estado enviando sus reportes mensuales o no envió su solicitud de reconocimiento de notas.'

        ]);
        PostulationCoevanState::create([
            'name' => 'Finalizado',
            'description' => 'Para pasar a un estado finalizado, se tiene que adjuntar la resolución del consejo universitario. Se finaliza la pasantía sin ninguna observaciones.'

        ]);
        PostulationCoevanState::create([
            'name' => 'Finalizado con Observación',
            'description' => 'Si se ha finalizado la pasantía con alguna observación.'

        ]);
        PostulationCoevanState::create([
            'name' => 'Anulado Por Postulante',
            'description' => 'Cuando se anula la postulación por parte del postulante, quizá porque ya no quiere participar en la convocatoria.'

        ]);
        PostulationCoevanState::create([
            'name' => 'Anulado Por Administrador',
            'description' => 'Cuando el administrador anula la postulación de esta persona por posible identificación de vulneración de reglas o intenta repetir su postulación, o detecta alguna mentira en sus datos incluidos.'

        ]);
        PostulationCoevanState::create([
            'name' => 'Postulación No Ganador',
            'description' => 'Cuando el postulante no logra ganar  o ser aceptado para la pasantía.'

        ]); 
    }
}
