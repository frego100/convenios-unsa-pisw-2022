<?php

namespace Database\Seeders;

use App\Models\Faculty;
use Illuminate\Database\Seeder;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faculty::create([
            'name'=>'Facultad de Ingenieria de Procesos',
            'acronym' => 'FIP',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ingenieria de Produccion y Servicios',
            'acronym' => 'FIPS',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Geologia, Geofisica y Minas',
            'acronym' => 'FGGM',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ingenieria Civil',
            'acronym' => 'FIC',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ciencias Naturales y Formales',
            'acronym' => 'FCNF',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Arquitectura y Urbanismo',
            'acronym' => 'FAU',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ciencias Biologicas',
            'acronym' => 'FCB',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Medicina',
            'acronym' => 'FM',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Enfermeria',
            'acronym' => 'FE',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Agronomia',
            'acronym' => 'FA',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ciencias Contables y Financieras',
            'acronym' => 'FCCF',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Economia',
            'acronym' => 'FE',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Derecho',
            'acronym' => 'FD',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Ciencias Historico Sociales',
            'acronym' => 'FIP',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Psicologia RRII Cs. de la Comunicacion',
            'acronym' => 'FPRICC',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Filosofia y Humanidades',
            'acronym' => 'FFH',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Administracion',
            'acronym' => 'FAD',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);

        Faculty::create([
            'name'=>'Facultad de Educacion',
            'acronym' => 'FED',
            'log_user_created' => 'admin',
            'log_user_modified' => 'admin'
        ]);
    }
}
