<?php

use App\Models\Profile;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('image')->nullable(true);
            $table->string('name')->nullable(true);
            $table->string('last_name')->nullable(true);
            $table->string('address')->nullable(true);
            $table->unsignedBigInteger('type')->nullable(true);
            $table->string('phone')->nullable(true);
            $table->unsignedBigInteger('identification')->nullable(true);
            $table->date('birthdate')->nullable(true);
            //$table->enum('profile_created',[Profile::COMPLETADO,Profile::NO_COMPLETADO])->default(Profile::NO_COMPLETADO);
            $table->integer('profile_created')->default(0);
            //$table->enum('log_status',[Profile::ACTIVO,Profile::INACTIVO,Profile::ELIMINADO])->default(Profile::ACTIVO);
            $table->integer('log_status')->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type')->references('id')->on('profile_types');
            $table->foreign('identification')->references('id')->on('user_identifications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
