<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('lastname');
            $table->string('address');
            $table->integer('phone');
            $table->string('email')->unique();
            $table->string('password');
            $table->unsignedBigInteger('role');
            $table->string('log_user_created')->nullable();
            $table->string('log_user_modified')->nullable();
            $table->timestamps();
            $table->integer('log_status')->default(0);
            $table->rememberToken();
            $table->foreign('role')->references('id')->on('admin_roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
