<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocations', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('correlative')->nullable();
            $table->unsignedBigInteger('type');
            $table->unsignedBigInteger('modality');
            $table->string('description')->nullable();
            $table->unsignedBigInteger('conv_state');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('important_notes')->nullable();
            $table->string('afiche')->nullable();
            $table->string('log_user_created')->nullable();
            $table->string('log_user_modified')->nullable();
            $table->timestamps();

            
            $table->integer('log_status')->default(0);
            $table->foreign('modality')->references('id')->on('modalities');
            $table->foreign('type')->references('id')->on('convocation_types');
            $table->foreign('conv_state')->references('id')->on('convocation_states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocations');
    }
}
