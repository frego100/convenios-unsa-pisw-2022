<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventPivTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_piv', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_type");
            $table->unsignedBigInteger("id_piv_convocation");

            $table->foreign("id_type")->references("id")->on("event_types");
            $table->foreign("id_piv_convocation")->references("id")->on("piv_convocations");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_piv');
    }
}
