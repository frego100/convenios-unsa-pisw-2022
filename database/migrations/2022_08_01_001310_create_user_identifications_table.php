<?php

use App\Models\UserIdentification;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_identifications', function (Blueprint $table) {
            $table->id();
            $table->string('value',30)->nullable(true);
            $table->unsignedBigInteger('type')->nullable(true);
            //$table->enum('log_status',[UserIdentification::ACTIVO,UserIdentification::INACTIVO,UserIdentification::ELIMINADO])->default(UserIdentification::ACTIVO);
            $table->integer('log_status')->default(0);
            $table->foreign('type')->references('id')->on('user_identification_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_identifications');
    }
}
