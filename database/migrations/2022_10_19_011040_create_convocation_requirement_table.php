<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvocationRequirementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocation_requirement', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("convocation_id");
            $table->unsignedBigInteger("requirement_id");

            $table->foreign("convocation_id")->references("id")->on("convocations");
            $table->foreign("requirement_id")->references("id")->on("requirements");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocation_requirement');
    }
}
