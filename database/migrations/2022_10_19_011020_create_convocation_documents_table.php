<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvocationDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocation_documents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_convocation');
            $table->unsignedBigInteger('id_documents');
            $table->string('log_user_created')->nullable();
            $table->string('log_user_modified')->nullable();
            $table->timestamps();
            $table->integer('log_status')->default(0);
            $table->foreign('id_convocation')->references('id')->on('convocations');
            $table->foreign('id_documents')->references('id')->on('documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocation_documents');
    }
}
