<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePivdoConvocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivdo_convocations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("event_type");
            $table->integer('log_status')->default(0);
            $table->timestamps();
            $table->foreign('event_type')->references('id')->on('event_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivdo_convocations');
    }
}
