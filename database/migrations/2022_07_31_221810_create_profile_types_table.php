<?php

use App\Models\ProfileType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_types', function (Blueprint $table) {
            $table->id();
            $table->string('description',30);
            //$table->enum('log_status',[ProfileType::ACTIVO,ProfileType::INACTIVO,ProfileType::ELIMINADO])->default(ProfileType::ACTIVO);
            $table->integer('log_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_types');
    }
}
