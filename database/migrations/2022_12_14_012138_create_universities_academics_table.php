<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUniversitiesAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities_academics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_university");
            $table->unsignedBigInteger("id_academic_network");

            $table->foreign("id_university")->references("id")->on("universities");
            $table->foreign("id_academic_network")->references("id")->on("academic_networks");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities_academics');
    }
}
