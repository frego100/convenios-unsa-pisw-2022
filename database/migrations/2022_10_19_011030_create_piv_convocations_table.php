<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePivConvocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piv_convocations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("id_convocation")->nullable(true);;
            $table->integer('log_status')->default(0);
            $table->timestamps();
            $table->foreign('id_convocation')->references('id')->on('convocations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piv_convocations');
    }
}
