<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulationCoevanCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulation_coevan_courses', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("id_postulation");
            $table->integer("number_credits");
            $table->string("course_code");
            $table->string("unsa_course_name");
            $table->string("year");
            $table->string("semester");
            $table->string("target_university_course_name");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulation_coevan_courses');
    }
}
