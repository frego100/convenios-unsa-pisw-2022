<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvocationCoevansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convocation_coevans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_convocation')->nullable(true);
            $table->unsignedBigInteger('id_academic_network')->nullable(true);
            $table->unsignedBigInteger('id_university')->nullable(true);
            $table->text('avaltext')->nullable(true);
            $table->text('coursestext')->nullable(true);
            $table->text('commitment')->nullable(true);            
            $table->string('semester')->nullable(true);
            $table->timestamps();

            
            $table->foreign('id_academic_network')->references('id')->on('academic_networks');
            $table->foreign('id_university')->references('id')->on('universities');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convocation_coevans');
    }
}
