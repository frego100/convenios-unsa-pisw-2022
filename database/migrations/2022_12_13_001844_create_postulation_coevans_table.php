<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulationCoevansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulation_coevans', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("id_convocation")->nullable(true);
            $table->integer("id_user")->nullable(true);
            $table->string("name")->nullable(true);
            $table->string("lastname")->nullable(true);
            $table->string("birth_date")->nullable(true);
            $table->string("dni")->nullable(true);
            $table->string("city_region_postulant")->nullable(true);
            $table->string("cui")->nullable(true);
            $table->string("current_address")->nullable(true);
            $table->string("phone")->nullable(true);
            $table->string("email")->nullable(true);
            $table->string("contact_emergency_number")->nullable(true);
            $table->unsignedInteger("origin_university")->nullable(true);
            $table->string("web_page")->nullable(true);
            $table->string("city_region_university")->nullable(true);
            $table->unsignedInteger("id_faculty")->nullable(true);
            $table->unsignedInteger("id_professional_program")->nullable(true);
            $table->unsignedInteger("id_current_cicle")->nullable(true);
            $table->unsignedInteger("id_academic_year")->nullable(true);
            $table->integer("mean_grades")->nullable(true);
            $table->integer("total_credits")->nullable(true);
            $table->string("coordinator")->nullable(true);
            $table->string("coordinator_cargue")->nullable(true);
            $table->string("coordinator_email")->nullable(true);
            $table->string("postulation_document")->nullable(true);
            $table->string("photo")->nullable(true);
            $table->string("last_update")->nullable(true);
            $table->unsignedInteger("post_state")->nullable(true);

            //$table->foreign('origin_university')->references('id')->on('universities');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulation_coevans');
    }
}
