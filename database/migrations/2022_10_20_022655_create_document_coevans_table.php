<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentCoevansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_coevans', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable(true);
            $table->unsignedBigInteger('type')->nullable(true);
            $table->string('url')->nullable(true);
            $table->string("description")->nullable(true);
            $table->unsignedBigInteger('id_convocation')->nullable(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_coevans');
    }
}
