<?php

use App\Models\UserIdentificationType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserIdentificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_identification_types', function (Blueprint $table) {
            $table->id();
            $table->string('description',30);
            //$table->enum('log_status',[UserIdentificationType::ACTIVO,UserIdentificationType::INACTIVO,UserIdentificationType::ELIMINADO])->default(UserIdentificationType::ACTIVO);
            $table->integer('log_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_identification_types');
    }
}
