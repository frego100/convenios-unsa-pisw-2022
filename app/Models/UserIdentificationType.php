<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserIdentificationType extends Model
{
    use HasFactory;

    protected $table = "user_identification_types";

    public function profile(){
        return $this->hasMany(UserIdentification::class);
    }

    protected $fillable = [
        'description','log_status'
    ];
}
