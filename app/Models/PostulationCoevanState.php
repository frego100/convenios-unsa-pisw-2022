<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostulationCoevanState extends Model
{
    use HasFactory;

    protected $table = "postulation_coevan_states";
    protected $fillable = [
        'name',
        'description'
    ];
}
