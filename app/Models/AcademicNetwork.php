<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicNetwork extends Model
{
    use HasFactory;
    protected $table = "academic_networks";

    public function convocationCoevan(){
        return $this->hasMany(ConvocationCoevan::class);
    }


    protected $fillable = [
        'name',
        'acronym',
        'description',
        'logo'
    ];
}
