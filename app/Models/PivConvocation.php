<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PivConvocation extends Model
{
    use HasFactory;

    protected $table ="piv_convocations";

    public function eventTypes(){
        return $this->belongsToMany(EventType::class,"event_piv","id_piv_convocation","id_type");
    } 

    public function convocation(){
        return $this->belongsTo(Convocation::class);
    }


    protected $fillable = [
        'event_type','log_status'
    ];
}
