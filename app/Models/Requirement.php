<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    use HasFactory;

    protected $table ="requirements";

    protected $guarded = ['id'];
    protected $fillable = [
        'description','log_status'
    ];

    public function convocations(){
        return $this->belongsToMany(Convocation::class,"convocation_requirement","requirement_id","convocation_id");
    }
}
