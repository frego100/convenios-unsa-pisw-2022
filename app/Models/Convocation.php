<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Convocation extends Model
{
    use HasFactory;
    protected $table = "convocations";
    protected $guarded = ['id'];
    protected $fillable = [
        'title',
        'correlative',
        'type',
        'modality',
        'description',
        'conv_state',
        'start_date',
        'end_date',
        'important_notes',
        'afiche'
    ];

    public function requirements(){
        return $this->belongsToMany(Requirement::class,"convocation_requirement","convocation_id","requirement_id");
    }

    public function pivConvocation(){
        return $this->hasOne(PivConvocation::class,"id_convocation");
    }

    public function convocationType(){
        return $this->belongsTo(ConvocationType::class);
    }

}
