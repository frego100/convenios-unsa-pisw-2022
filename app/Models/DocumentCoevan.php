<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentCoevan extends Model
{
    use HasFactory;

    protected $table = "document_coevans";


    protected $fillable = [
        'id_convocation','name','type','description','url'
    ];
}
