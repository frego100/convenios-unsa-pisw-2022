<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserIdentification extends Model
{
    use HasFactory;

    protected $table = "user_identifications";
    protected $with = ['userIdentificationType'];


    public function userIdentificationType(){
        return $this->belongsTo(UserIdentificationType::class,'type');
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    protected $fillable = [
        'value','type','log_status'
    ];

}
