<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExternalStudent extends Model
{
    use HasFactory;
    protected $table = "external_students";

    protected $fillable = [
        'name',
        'lastname',
        'email',
        'justification'
    ];
}
