<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvocationDocument extends Model
{
    use HasFactory;
    protected $table = "convocation_documents";

    protected $fillable = [
        'id_convocation',
        'id_documents'
    ];
}
