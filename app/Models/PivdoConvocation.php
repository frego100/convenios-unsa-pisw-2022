<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PivdoConvocation extends Model
{
    use HasFactory;
    protected $table ="pivdo_convocations";

    public function eventType(){
        return $this->belongsTo(EventType::class);
    } 
}
