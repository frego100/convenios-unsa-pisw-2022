<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvocationRequirement extends Model
{
    use HasFactory;

    protected $table ="requirements";


    protected $fillable = [
        'description','log_status'
    ];
    
}
