<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileType extends Model
{
    use HasFactory;
    protected $table = "profile_types";

    public function profile(){
        return $this->hasMany(Profile::class);
    }

    protected $fillable = [
        'description','log_status'
    ];

}
