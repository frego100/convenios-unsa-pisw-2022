<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = "profiles";


    public function profileType(){
        return $this->belongsTo(ProfileType::class,'type');
    }

    public function userIdentification(){
        return $this->belongsTo(UserIdentification::class,'identification');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    protected $fillable = [
        //'description',
        'image',
        'name',
        'last_name',
        'address',
        'type',
        'phone',
        'identification',
        'birthdate',
        'profile_created',
        'log_status'
    ];
}
