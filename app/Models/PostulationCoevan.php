<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostulationCoevan extends Model
{
    use HasFactory;

    protected $table = "postulation_coevans";

    public function university(){
        return $this->hasMany(University::class,'origin_university');
    }
    
    protected $fillable = [   
        'id_convocation',
        'id_user',
        'name',
        'lastname',
        'birth_date',
        'dni',
        'city_region_postulant',
        'cui',
        'current_address',
        'phone',
        'email',
        'contact_emergency_number',
        'origin_university',
        'web_page',
        'city_region_university',
        'id_faculty',
        'id_professional_program',
        'id_current_cicle',
        'id_academic_year',
        'mean_grades',
        'total_credits',
        'coordinator',
        'coordinator_cargue',
        'postulation_document',
        'photo',
        'last_update',
        'post_state',
    ];

}
