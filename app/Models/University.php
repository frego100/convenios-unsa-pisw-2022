<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use HasFactory;
    protected $table = "universities";

    public function university(){
       return $this->hasMany(University::class);
    }


    protected $fillable = [
        'name',
        'acronym',
        'logo',
    ];
}
