<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvocationType extends Model
{
    use HasFactory;
    protected $table ="convocation_types";

    protected $fillable = [
        'name','acronym','log_status'
    ];

    public function convocations(){
        return $this->hasMany(Convocation::class,"type");
    }
}
