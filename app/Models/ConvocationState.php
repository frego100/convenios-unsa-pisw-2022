<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvocationState extends Model
{
    use HasFactory;
    protected $table = "convocation_states";
    

    protected $fillable = [
        'name',
        'description'
    ];
}
