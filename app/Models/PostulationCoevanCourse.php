<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostulationCoevanCourse extends Model
{
    use HasFactory;
    protected $table = "postulation_coevan_courses";

    protected $fillable = [
        'id_postulation',
        'number_credits',
        'course_code',
        'unsa_course_name',
        'year',
        'semester',
        'target_university_course_name',

    ];
}
