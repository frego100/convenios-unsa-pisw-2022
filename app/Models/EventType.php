<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    use HasFactory;

    protected $table ="event_types";
/*
    public function pivdoConvocation(){
        return $this->hasMany(PivdoConvocation::class);
    } 

    public function piveConvocation(){
        return $this->hasMany(PiveConvocation::class);
    } 
*/
    public function pivConvocation(){
        return $this->belongsToMany(PivConvocation::class,"event_piv","id_type","id_piv_convocation");
    } 


    protected $fillable = [
        'name','log_status'
    ];

    
}
