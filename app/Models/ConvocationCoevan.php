<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConvocationCoevan extends Model
{
    use HasFactory;

    protected $table = "convocation_coevans";

    public function academicNetwork(){
        return $this->belongsTo(AcademicNetwork::class,'id_academic_network');
    }

    public function university(){
        return $this->belongsTo(University::class,'id_university');
    }

    protected $fillable = [
        'id_convocation','id_academic_network','id_university'
    ];
}
