<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRoles extends Model
{
    use HasFactory;
    protected $table = "admin_roles";

    protected $fillable = [
        'name',
        'log_user_created',
        'log_user_modified'
    ];
}
