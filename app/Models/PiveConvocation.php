<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PiveConvocation extends Model
{
    use HasFactory;

    protected $table ="pive_convocations";

    public function eventType(){
        return $this->belongsTo(EventType::class);
    } 
}
