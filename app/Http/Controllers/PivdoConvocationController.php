<?php

namespace App\Http\Controllers;

use App\Models\PivdoConvocation;
use Illuminate\Http\Request;

class PivdoConvocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>PivdoConvocation::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pivdoConv = new PivdoConvocation();
        $pivdoConv->event_type = $request->event_type;
        $pivdoConv->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$pivdoConv
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>PivdoConvocation::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PivdoConvocation  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PivdoConvocation $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

}
