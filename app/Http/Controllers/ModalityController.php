<?php

namespace App\Http\Controllers;

use App\Models\Modality;
use Illuminate\Http\Request;

class ModalityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modalities = Modality::where("log_status", "=", "0")->get();
        
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$modalities
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required'
        ]);
        
        $current_user = "admin";
        
        $modality = new Modality();
        $modality->name = $request->name;
        $modality->log_user_created = $current_user;
        $modality->log_user_modified = $current_user;
        $modality->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $modality
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modality  $modality
     * @return \Illuminate\Http\Response
     */
    public function show(Modality $modality)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modality  $modality
     * @return \Illuminate\Http\Response
     */
    public function edit(Modality $modality)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modality  $modality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modality $modality)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modality  $modality
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $modality = Modality::find($id);
        if($modality) {
            $modality->log_status = 1;
            $modality->save();

            return response()->json([
                'code'=>200,
                'msg'=>'eliminado',
                'data'=>$modality
            ]);
        }
    }
}
