<?php

namespace App\Http\Controllers;

use App\Models\ConvocationType;
use Illuminate\Http\Request;

class ConvocationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ConvocationType::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $convocationType = new ConvocationType();
        $convocationType->name = $request->name;
        $convocationType->acronym = $request->acronym;
        $convocationType->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$convocationType
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>ConvocationType::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ConvocationType  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ConvocationType $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

}
