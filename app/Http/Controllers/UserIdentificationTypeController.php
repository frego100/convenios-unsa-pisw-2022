<?php

namespace App\Http\Controllers;

use App\Models\UserIdentificationType;
use Illuminate\Http\Request;

class UserIdentificationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>UserIdentificationType::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userIdentificationType = new UserIdentificationType();
        $userIdentificationType->description = $request->description;
        $userIdentificationType->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$userIdentificationType,201
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>UserIdentificationType::findOrFail($id)
        ]);
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ProfileType  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,UserIdentificationType $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
