<?php

namespace App\Http\Controllers;

use App\Models\PostulationCoevanState;
use Illuminate\Http\Request;

class PostulationCoevanStateController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>PostulationCoevanState::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $postulationCoevanState = new PostulationCoevanState();
        $postulationCoevanState->name = $request->name;
        $postulationCoevanState->description = $request->description;
        $postulationCoevanState->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$postulationCoevanState,201
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>PostulationCoevanState::findOrFail($id)
        ]);
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PostulationCoevanState  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PostulationCoevanState $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
