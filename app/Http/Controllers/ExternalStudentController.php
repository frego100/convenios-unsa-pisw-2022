<?php

namespace App\Http\Controllers;

use App\Http\Resources\ExternalStudentResource;
use App\Models\ExternalStudent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ExternalStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ExternalStudentResource::collection(DB::table('external_students')->orderBy('status_request','asc')->get())
        ]);
    }

    public function remainingRequests()
    {
        $remaining = ExternalStudent::where("status_request", "=", "0")->count();
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$remaining
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'lastname' => 'required',
            'justification' => 'required',
            'email' => 'required|unique:external_students'
        ]);
        
        $external_student = new ExternalStudent();
        $external_student->name = $request->name;
        $external_student->lastname = $request->lastname;
        $external_student->email = $request->email;
        $external_student->justification = $request->justification;
        // guardar la justificacion (file)
        //$external_student->justification = $request->justification;
         //GUARDAR Justificacion EN S3
         $path = "justificationExternalStudent";
         $path=Storage::disk('s3')->put($path,$request->justification,'public');
         $external_student->justification = $path;
        
        $external_student->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $external_student
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExternalStudent  $externalStudent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $external_student = new ExternalStudentResource(ExternalStudent::findOrFail($id));
        
        if($external_student) {
            return response()->json([
                'code' => 200,
                'msg'=> 'ok',
                'data'=> $external_student
            ]);
        }else {
            return response()->json([
                'code' => 500,
                'msg'=> 'External Student not found',
                'data'=> ''
            ]);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExternalStudent  $externalStudent
     * @return \Illuminate\Http\Response
     */
    public function edit(ExternalStudent $externalStudent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExternalStudent  $externalStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $external_student = ExternalStudent::find($request->id);
        if($external_student) {
            $external_student->password_created = $request->password_created;
            $external_student->status_request = 1;
            $external_student->save();

            return response()->json([
                'code'=>200,
                'msg'=>'Solicitud aceptada',
                'data'=>$external_student
            ]);
        }else {
            return response()->json([
                'code'=>500,
                'msg'=>'External Student not found',
                'data'=>''
            ]);
        }
    }

    public function reject(Request $request)
    {
        $external_student = ExternalStudent::find($request->id);
        if($external_student) {
            $external_student->status_request = 2;
            $external_student->save();

            return response()->json([
                'code'=>200,
                'msg'=>'Solicitud rechazada',
                'data'=>$external_student
            ]);
        }else {
            return response()->json([
                'code'=>500,
                'msg'=>'External Student not found',
                'data'=>''
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExternalStudent  $externalStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $external_student = ExternalStudent::findOrFail($id);
        $external_student -> delete();
        return response()->json([
            'code'=>200,
            'msg'=>'eliminado'
        ]);
    }
}
