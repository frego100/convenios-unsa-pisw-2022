<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostulationCoevanResource;
use App\Models\AcademicYear;
use App\Models\CurrentCycle;
use App\Models\Faculty;
use App\Models\PostulationCoevan;
use App\Models\PostulationCoevanCourse;
use App\Models\PostulationCoevanState;
use App\Models\Program;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostulationCoevanController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>PostulationCoevanResource::collection(PostulationCoevan::all())
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //logger($request->all());
        $postulationCoevan = new PostulationCoevan();
        $postulationCoevan->id_convocation = $request->id_convocation;
        $postulationCoevan->id_user = $request->id_user;
        $postulationCoevan->name = $request->name;
        $postulationCoevan->lastname = $request->lastname;
        $postulationCoevan->birth_date = $request->birth_date;
        $postulationCoevan->dni= $request->dni;
        $postulationCoevan->city_region_postulant = $request->city_region_postulant;
        $postulationCoevan->cui = $request->cui;
        $postulationCoevan->current_address = $request->current_address;
        $postulationCoevan->phone = $request->phone;
        $postulationCoevan->email = $request->email;
        $postulationCoevan->contact_emergency_number = $request->contact_emergency_number;
        $postulationCoevan->origin_university = $request->origin_university;
        $postulationCoevan->web_page = $request->web_page;
        $postulationCoevan->city_region_university = $request->city_region_university;
        $postulationCoevan->id_faculty = $request->id_faculty;
        $postulationCoevan->id_professional_program = $request->id_professional_program;
        $postulationCoevan->id_current_cicle = $request->id_current_cicle;
        $postulationCoevan->id_academic_year = $request->id_academic_year;
        $postulationCoevan->mean_grades = $request->mean_grades;
        $postulationCoevan->total_credits = $request->total_credits;
        $postulationCoevan->coordinator = $request->coordinator;
        $postulationCoevan->coordinator_cargue = $request->coordinator_cargue;
        $postulationCoevan->coordinator_email = $request->coordinator_email;

        //GUARDAR LOGO EN S3
        $path = "postulationDocument";
        $path=Storage::disk('s3')->put($path,$request->postulation_document,'public');
        $postulationCoevan->postulation_document = $path;
        //$postulationCoevan->postulation_document = $request->postulation_document;

        //GUARDAR LOGO EN S3
        $pathF = "postulationPhoto";
        $pathF=Storage::disk('s3')->put($pathF,$request->photo,'public');
        $postulationCoevan->photo= $pathF;
        //$postulationCoevan->postulation_document = $request->postulation_document;
 
        $postulationCoevan->last_update = $request->last_update;
        $postulationCoevan->post_state = $request->post_state;
        $postulationCoevan->save();

        $courses_postulation = $request->courses;
        foreach($courses_postulation as $l){
            $l = json_decode($l,true);
            $pcc = new PostulationCoevanCourse();
            $pcc->id_postulation = $postulationCoevan->id;
            $pcc-> number_credits= $l["number_credits"];
            $pcc->course_code = $l["course_code"];
            $pcc->unsa_course_name = $l["unsa_course_name"];
            $pcc->year = $l['year'];
            $pcc->semester = $l['semester'];
            $pcc->target_university_course_name = $l['target_university_course_name'];
            $pcc->save();
        }        

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$postulationCoevan,201
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        $postulationCoevan = PostulationCoevan::find($id);

        if($postulationCoevan==null){
            return response()->json([
                'code' => 404,
                'msg' => 'PostulationCoevan not found',
                'data'=> []
            ]);
        }
        else{          
            return response()->json([
                'code'=>200,
                'msg'=>'ok',
                'data'=> new PostulationCoevanResource($postulationCoevan)
            ]); 
        }        
    }

    public function showPostulationByConvocationUser(Request $request)
    {
        $id_user = $request->id_user;
        $id_convocation = $request->id_convocation;

        $postulationCoevan = PostulationCoevan::where('id_user',$id_user)->where('id_convocation',$id_convocation)->first();
    
        if($postulationCoevan==null){
            return response()->json([
                'code' => 404,
                'msg' => 'PostulationCoevan not found',
                'data'=> []
            ]);
        }
        else{          
            return response()->json([
                'code'=>200,
                'msg'=>'ok',
                'data'=> new PostulationCoevanResource($postulationCoevan)           
            ]); 
        }       
    }


    public function showPostulationByConvocation(Request $request)
    {
        $id_convocation = $request->id_convocation;
        $postulationCoevan = PostulationCoevan::where('id_convocation',$id_convocation)->get();
    
        if($postulationCoevan==null){
            return response()->json([
                'code' => 404,
                'msg' => 'PostulationCoevan not found',
                'data'=> []
            ]);
        }
        else{          
            return response()->json([
                'code'=>200,
                'msg'=>'ok',
                'data'=> PostulationCoevanResource::collection($postulationCoevan)        
            ]); 
        }       
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PostulationCoevan  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PostulationCoevan $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
