<?php

namespace App\Http\Controllers;

use App\Models\AdminRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>DB::table('admin_roles')->orderBy('id','asc')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // this is a function to create method to save a role.
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required'
        ]);
        
        $current_user = "admin";
        
        $admin_role = new AdminRoles();
        $admin_role->name = $request->name;
        $admin_role->log_user_created = $current_user;
        $admin_role->log_user_modified = $current_user;
        $admin_role->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $admin_role
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function show(AdminRoles $adminRoles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminRoles $adminRoles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminRoles $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdminRoles  $adminRoles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			AdminRoles::find($id); 
    }
}
