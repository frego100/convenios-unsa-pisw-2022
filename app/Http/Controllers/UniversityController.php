<?php

namespace App\Http\Controllers;

use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class UniversityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $universities = University::where("log_status", "=", "0")->get();
        
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$universities
        ]);  
    }

    public function getByAcademicNetwork(Request $request)
    {
        $result = DB::select('select id_university from universities_academics where id_academic_network = :id', ['id' => $request->id]);
        
        // create array from query result
        $universities_id = [];

        foreach($result as $r){
            array_push($universities_id, $r->id_university);
        }
        
        $unis = DB::table('universities')->whereIn('id', $universities_id)->get();
        
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$unis
        ]);
    }

    public function assignAcademicNetwork(Request $request) {
        $request->validate([
            'id_university' => 'required',
            'id_academic_network' => 'required'
        ]);

        $date = \Carbon\Carbon::now();

        DB::insert('insert into universities_academics (id_university, id_academic_network, created_at, updated_at) values (?, ?, ?, ?)', [$request->id_university, $request->id_academic_network, $date, $date]);

        return response()->json([
            'code' => 200,
            'msg' => 'ok',
            'data' => 'successfull'
        ]);      
    }

    public function listAllAssigns() {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>DB::table('universities_academics')->orderBy('id','asc')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'acronym' => 'required',
            'logo' => 'required',
        ]);
        
        $current_user = "admin";
        
        $university = new University();
        $university->name = $request->name;
        $university->acronym = $request->acronym;

        // guardar el logo de la universidad
        // $university->logo = $request->logo;
         //GUARDAR LOGO EN S3
         $path = "logoUniversity";
         $path=Storage::disk('s3')->put($path,$request->logo,'public');
         $university->logo = $path;

        $university->log_user_created = $current_user;
        $university->log_user_modified = $current_user;
        $university->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $university
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\University  $university
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $university=University::find($id);
        
        if($university != null){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> $university 
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'University not found',
                'data'=> ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\University  $university
     * @return \Illuminate\Http\Response
     */
    public function edit(University $university)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\University  $university
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, University $id)
    {
        //
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'successfull',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\University  $university
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $university = University::find($id);
        if($university) {
            $university->log_status = 1;
            $university->save();

            return response()->json([
                'code'=>200,
                'msg'=>'eliminado',
                'data'=>$university
            ]);
        }
    }
}
