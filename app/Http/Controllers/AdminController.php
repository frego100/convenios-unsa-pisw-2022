<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\AdminRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::where("log_status", "=", "0")->get();
        
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$admins
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'lastname'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'email'=>'required|unique:admins',
            'password'=>'required',
            'role'=>'required'
        ]);

        $current_user = "admin";

        $admin = new Admin();
        $admin->name = $request->name;
        $admin->lastname = $request->lastname;
        $admin->address = $request->address;
        $admin->phone = $request->phone;
        $admin->email = $request->email;
        $admin->password = $request->password;
        $admin->role = $request->role;
        $admin->log_user_created = $current_user;
        $admin->log_user_modified = $current_user;
        $admin->save();
        
        $token = $admin->createToken("auth_token")->plainTextToken;

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> [
                'admin' => ['id'=>$admin->id,
                            'name' => $admin->name,
                            'lastname' => $admin->lastname,
                            'address' => $admin->address,
                            'phone' => $admin->phone, 
                            'email' => $admin->email
                        ],
                'token' => $token
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //search by id
        $admin = Admin::find($request->id);
        $role = AdminRoles::find($admin->role);
        
        if($admin != null){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> [
                    'id'=>$admin->id,
                    'name' => $admin->name,
                    'lastname' => $admin->lastname,
                    'address' => $admin->address,
                    'phone' => $admin->phone, 
                    'email' => $admin->email,
                    'password' => $admin->password,
                    'role' => [
                        'id' => $role->id,
                        'name' => $role->name
                    ]
                ]
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'Admin not found',
                'data' => ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $id)
    {
        
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::find($id);
        if($admin) {
            $admin->log_status = 1;
            $admin->save();
        }

        return response()->json([
            'code'=>200,
            'msg'=>'eliminado',
            'data'=>$admin
        ]);
    }

    public function login(Request $request)
    {
        $admin = Admin::where("email", "=", $request->email)->first();
        $role = AdminRoles::find($admin->role);

        if($admin == null || $admin->password != $request->password){
            return response()->json([
                'code' => 400,
                'msg'=> 'datos incorrectos',
                'data'=> []
            ]); 
        }else {
            //creamos el token
            $token = $admin->createToken("auth_token")->plainTextToken;
            //si está todo ok
            return response()->json([
                'code' => 200,
                'msg'=> 'Successful login',
                'data'=> [
                    'user' => ['id' => $admin->id, 
                               'email' => $admin->email,
                               'role' => [
                                    'id' => $role->id,
                                    'name' => $role->name]
                            ],
                    'token' => $token
                ]
            ]);
        } 
/*
        if( isset($admin->id) ){
            
                //creamos el token
                $token = $admin->createToken("auth_token")->plainTextToken;
                //si está todo ok
                return response()->json([
                    'code' => 200,
                    'msg'=> 'Successful login',
                    'data'=> [
                        'user' => ['id' => $admin->id, 
                                   'email' => $admin->email],
                        'token' => $token
                    ]
                ]);
        }/*else{
            /*
            return response()->json([
                "status" => 0,
                "msg" => "Usuario no registrado",
            ], 404);  

            return response()->json([
                'code' => 400,
                'msg'=> 'no existe',
                'data'=> [
                    'user' => ['id' => $admin->id, 
                               'email' => $admin->email],
                    //'token' => $token
                ]
            ]);
        }*/
    }

    public function logout(Request $request)
    {
        $request->admin()->currentAccessToken()->delete();
        return response()->json(
            [
                'code' => 200,
                'msg' => 'Logout successfully'
            ]
        );
    }
}
