<?php

namespace App\Http\Controllers;

use App\Models\Convocation;
use App\Models\PivConvocation;
use App\Models\Requirement;
use Illuminate\Http\Request;

class PivConvocationController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>PivConvocation::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pivConv = new PivConvocation();
        $pivConv->id_convocation = $request->id_convocation;
        $pivConv->save();
        
        $pivConv->eventTypes()->attach($request->event_types);
        
        $req= Convocation::find($request->id_convocation);
     
        $req->requirements()->attach($request->requirements);
        

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$pivConv
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>PivConvocation::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PiveConvocation  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PivConvocation $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);

    }
}
