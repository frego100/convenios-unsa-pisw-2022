<?php

namespace App\Http\Controllers;

use App\Models\ConvocationState;
use Illuminate\Http\Request;

class ConvocationStateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code' => 200,
            'msg' => 'ok',
            'data' => ConvocationState::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);
        
        $convocation_state = new ConvocationState();
        $convocation_state->name = $request->name;
        $convocation_state->description = $request->description;
        $convocation_state->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $convocation_state
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ConvocationState  $convocationState
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ConvocationState::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ConvocationState  $convocationState
     * @return \Illuminate\Http\Response
     */
    public function edit(ConvocationState $convocationState)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConvocationState  $convocationState
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConvocationState $convocationState)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConvocationState  $convocationState
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConvocationState $convocationState)
    {
        //
    }
}
