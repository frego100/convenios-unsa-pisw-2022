<?php

namespace App\Http\Controllers;

use App\Models\DocumentType;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>DocumentType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'description' => 'required|unique:document_types'
        ]);
        
        $current_user = "admin";
        
        $document_type = new DocumentType();
        $document_type->description = $request->description;
        $document_type->log_user_created = $current_user;
        $document_type->log_user_modified = $current_user;
        $document_type->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $document_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //search by id
        $document_type = DocumentType::find($id);
        
        if($document_type != null && $document_type->log_status == 0){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> $document_type,
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'Document type not found',
                'data' => ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentType $documentType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentType $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document_type = DocumentType::find($id);
        if($document_type) {
            $document_type->log_status = 2;
            $document_type->save();
        }

        return response()->json([
            'code'=>200,
            'msg'=>'eliminado',
            'data'=>$document_type
        ]);

    }
}
