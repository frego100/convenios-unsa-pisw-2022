<?php

namespace App\Http\Controllers;

use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>Document::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'path' => 'required',
            'description' => 'required',
            'type' => 'required'
        ]);

        $current_user = "admin";
        
        $Document = new Document();
        //$path_to_save = $request->path->store('convocationDocuments','public');
        //$Document->path = $path_to_save;
        $path = "convocationDocuments";
        $path=Storage::disk('s3')->put($path,$request->path,'public');
        $Document->path = $path;

        $Document->description = $request->description;
        $Document->type = $request->type;
        $Document->log_user_created = $current_user;
        $Document->log_user_modified = $current_user;
        $Document->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $Document
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //search by id
        $document = Document::find($id);
        
        if($document != null && $document->log_status == 0){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> $document,
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'Document not found',
                'data' => ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Document::find($id);
        if($document){
            $document->update($request->all());
            
            return response()->json([
                'code'=>200,
                'msg'=>'exitoso',
                'data'=>$document
            ]);
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::find($id);
        if($document) {
            $document->log_status = 2;
            $document->save();
        }

        return response()->json([
            'code'=>200,
            'msg'=>'eliminado',
            'data'=>$document
        ]);
    }
}
