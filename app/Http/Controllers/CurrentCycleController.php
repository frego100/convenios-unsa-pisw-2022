<?php

namespace App\Http\Controllers;

use App\Models\CurrentCycle;
use Illuminate\Http\Request;

class CurrentCycleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>CurrentCycle::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'description' => 'required'
        ]);
        
        $current_user = "admin";
        
        $current_cycle = new CurrentCycle();
        $current_cycle->description = $request->description;
        $current_cycle->log_user_created = $current_user;
        $current_cycle->log_user_modified = $current_user;
        $current_cycle->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $current_cycle
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CurrentCycle  $currentCycle
     * @return \Illuminate\Http\Response
     */
    public function show(CurrentCycle $currentCycle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CurrentCycle  $currentCycle
     * @return \Illuminate\Http\Response
     */
    public function edit(CurrentCycle $currentCycle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CurrentCycle  $currentCycle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurrentCycle $currentCycle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CurrentCycle  $currentCycle
     * @return \Illuminate\Http\Response
     */
    public function destroy(CurrentCycle $currentCycle)
    {
        //
    }
}
