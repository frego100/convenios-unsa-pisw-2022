<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\ExternalStudent;
use App\Models\Profile;
use App\Models\User;
use App\Models\UserIdentification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //to save an user
    public function store(Request $request)
    {
        $varr = $request->validate([
            'email'=>'required|unique:users|regex:/(.*)@unsa.edu.pe/'
            //'image'=>'required'
        ]);

        $current_user = "admin";

        $user = new User();
        $user->email = $request->email;
        $user->log_user_created = $current_user;
        $user->log_user_modified = $current_user;
        $user->save();

        $userIdenti = new UserIdentification();
        $userIdenti->save();

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->image = $request->image;
        $profile->identification=$userIdenti->id;
        $profile->save();

        

        
        $token = $user->createToken("auth_token")->plainTextToken;

               
        //event(new Registered($user));
        //$token = $user->createToken('authtoken')->plainTextToken;
        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> [
                'user' => ['id' => $user->id, 
                           'email' => $user->email],
                'token' => $token
            ]
        ]);
    }

    public function login(Request $request)
    {

        $user = User::where("email", "=", $request->email)->first();

        if( isset($user->id) ){
            
                //creamos el token
                $token = $user->createToken("auth_token")->plainTextToken;
                //si está todo ok

                /*
                return response()->json([
                    "status" => 1,
                    "msg" => "¡Usuario logueado exitosamente!",
                    "access_token" => $token
                ]);*/    
                
                return response()->json([
                    'code' => 200,
                    'msg'=> 'ok',
                    'data'=> [
                        'user' => ['id' => $user->id, 
                                   'email' => $user->email],
                        'token' => $token
                    ]
                ]);
            

        }else{
            /*
            return response()->json([
                "status" => 0,
                "msg" => "Usuario no registrado",
            ], 404);  
*/
            return response()->json([
                'code' => 400,
                'msg'=> 'no existe',
                'data'=> [
                    'user' => ['id' => $user->id, 
                               'email' => $user->email],
                    //'token' => $token
                ]
            ]);
        } 

        /*
        
        $email_to_search = $request->email;
        $user = User::where('email','=',$email_to_search)->get();
        
        if(count($user) == 1){ // user valid
            $token = $user[0]->createToken('authtoken');
            return response()->json([
                'code' => 200,
                'msg'=> 'ok',
                'data'=> [
                    'user' => ['id' => $user[0]->id,
                               'email' => $user[0]->email],
                    'token' => $token->plainTextToken
                ]
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'User not found',
                'data'=> [
                    'user' => ['id' => '',
                               'email' => ''],
                    'token' => ''
                ]
            ]);
        }
*/
        
    }

    public function logout(Request $request){
       // $request->user()->tokens()->delete();
        $request->user()->currentAccessToken()->delete();
        return response()->json(
            [
                'code' => 200,
                'msg' => 'Logout successfully'
            ]
        );
    }

    public function externalStudentLogin(Request $request)
    {
        $ExternalStudent = ExternalStudent::where("email", "=", $request->email)->first();

        if($ExternalStudent == null || $ExternalStudent->password_created != $request->password){
            return response()->json([
                'code' => 400,
                'msg'=> 'Datos incorrectos'
            ]); 
        }else {
            return response()->json([
                'code' => 200,
                'msg'=> 'Login exitoso'
            ]);
        } 
    }

    public function show($id)
    {
        $user=User::find($id);
        
        if($user != null){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> [
                    'user' => ['id' => $user->id,
                               'email' => $user->email]
                ]
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'User not found',
                'data'=> [
                    'user' => ['id' => '',
                               'email' => '']
                ]
            ]);
        }
        

    }
}
