<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConvocationResource;
use App\Models\Convocation;
use App\Models\Base;
use App\Models\ConvocationDocument;
use App\Models\ConvocationState;
use App\Models\ConvocationType;
use App\Models\Document;
use App\Models\Modality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ConvocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ConvocationResource::collection(Convocation::all())
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required',
            'correlative' => 'required',
            'type' => 'required',
            'modality' => 'required',
            'description' => 'required',
            'conv_state' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'important_notes' => 'required',             
        ]);

        $current_user = "admin";
/*
        // CREAR Y GUARDAR DOCUMENTO
        $Afiche = new Document();
        //$afiche_path = $request->afiche->store('convocationDocuments','public');
        //$Afiche->path = $afiche_path;

        //GUARDAR IMAGEN EN S3
        $path = "convocationDocuments";
        $path=Storage::disk('s3')->put($path,$request->afiche,'public');
        $Afiche->path = $path;

        

        $Afiche->description = "convocation afiche";
        $Afiche->type = 2; // afiches
        $Afiche->log_user_created = $current_user;
        $Afiche->log_user_modified = $current_user;
        $Afiche->save();
*/

        // create convocation
        $convocation = new Convocation();
        $convocation->title = $request->title;
        $convocation->correlative = $request->correlative;
        $convocation->type = $request->type;
        $path = "convocationAfiche";
        $path=Storage::disk('s3')->put($path,$request->afiche,'public');
        $convocation->afiche = $path;
        $convocation->modality = $request->modality;
        $convocation->description = $request->description;
        $convocation->conv_state = $request->conv_state;
        $convocation->start_date = $request->start_date;
        $convocation->end_date = $request->end_date; 
        $convocation->important_notes = $request->important_notes;       
        $convocation->log_user_created = $current_user;
        $convocation->log_user_modified = $current_user;
        $convocation->save();
/*
        // convocation documents table
        $convocation_document_afiche = new ConvocationDocument();
        $convocation_document_afiche->id_convocation = $convocation->id; // id convocatoria
        //$convocation_document_afiche->id_documents = $Afiche->id;        // id documento
        $convocation_document_afiche->log_user_created = $current_user;
        $convocation_document_afiche->log_user_modified = $current_user;
        $convocation_document_afiche->save();
  */      
        $type = ConvocationType::find($convocation->type);
        $modality = Modality::find($convocation->modality);
        $state = ConvocationState::find($convocation->conv_state);

        return response()->json([
            'code' => 200,
            'msg' => 'ok',
            'data' => [
                'id' => $convocation->id,
                /*
                'title' => $convocation->title,
                'correlative' => $convocation->correlative,
                'type' => [
                    'id' => $type->id,
                    'name' => $type->name,
                    'acronym' => $type->acronym
                ],
                'modality' => [
                    'id' => $modality->id,
                    'name' => $modality->name
                ],
                'description' => $convocation->description,
                'conv_state' => [
                    'id' => $state->id,
                    'name' => $state->name,
                    'description' => $state->description
                ],
                'start_date' => $convocation->start_date,
                'end_date' => $convocation->end_date,
                'important_notes' => $convocation->important_notes,
                'afiche' => $convocation->afiche*/
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Convocation  $convocation
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $convocation = Convocation::find($request->id);
         
        if ($convocation == null)
        {
            return response()->json([
                'code' => 403,
                'msg' => 'convocation not found',
                'data' => []
            ]); 
        }

        $type = ConvocationType::find($convocation->type);
        $modality = Modality::find($convocation->modality);
        $state = ConvocationState::find($convocation->conv_state);
        
        $afiche= Storage::disk('s3') -> url($convocation->afiche);

        return response()->json([
            'code' => 200,
            'msg' => 'ok',
            'data' => [
                'id' => $convocation->id,
                'title' => $convocation->title,
                'correlative' => $convocation->correlative,
                'type' => [
                    'id' => $type->id,
                    'name' => $type->name,
                    'acronym' => $type->acronym
                ],
                'modality' => [
                    'id' => $modality->id,
                    'name' => $modality->name
                ],
                'description' => $convocation->description,
                'conv_state' => [
                    'id' => $state->id,
                    'name' => $state->name,
                    'description' => $state->description
                ],
                'important_notes' => $convocation->important_notes,
                'start_date' => $convocation->start_date,
                'end_date' => $convocation->end_date,
                'afiche' => $afiche
            ]
        ]);
    }

    public function showDetail(Request $request)
    {
        $convocation = Convocation::find($request->id);
        //$detail = PivConvocation::find($convocation->id_detail); 

        if ($convocation == null){
            return response()->json([
                'code' => 403,
                'msg' => 'convocation not found',
                'data' => []
            ]);    
        }

        return response()->json([
            'code' => 200,
            'msg' => 'showing detail',
            //'data' => $detail
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Convocation  $convocation
     * @return \Illuminate\Http\Response
     */
    public function edit(Convocation $convocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Convocation  $convocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Convocation $convocation)
    {
        //
    }

    public function changeState(Request $request)
    {
        $convocation = Convocation::find($request->convocation_id);
        
        if ($convocation == null)
        {
            return response()->json([
                'code' => 403,
                'msg' => 'convocation not found',
                'data' => []
            ]); 
        }
        
        $type = ConvocationType::find($convocation->type);
        $modality = Modality::find($convocation->modality);
        $state = ConvocationState::find($request->state_id);
        
        if($convocation) {
            $convocation->conv_state = $request->state_id;
            $convocation->save();            

            return response()->json([
                'code' => 200,
                'msg' => "Se ha cambiado el estado de la convocatoria exitosamente",
                'data' => [
                    'id' => $convocation->id,
                    'title' => $convocation->title,
                    'correlative' => $convocation->correlative,
                    'type' => [
                        'id' => $type->id,
                        'name' => $type->name,
                        'acronym' => $type->acronym
                    ],
                    'modality' => [
                        'id' => $modality->id,
                        'name' => $modality->name
                    ],
                    'description' => $convocation->description,
                    'conv_state' => [
                        'id' => $state->id,
                        'name' => $state->name,
                        'description' => $state->description
                    ],
                    'important_notes' => $convocation->important_notes,
                    'start_date' => $convocation->start_date,
                    'end_date' => $convocation->end_date,
                    'afiche' => $convocation->afiche
                ]  
            ]);
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Convocation  $convocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Convocation $convocation)
    {
        //
    }
}
