<?php

namespace App\Http\Controllers;

use App\Models\PiveConvocation;
use Illuminate\Http\Request;

class PiveConvocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>PiveConvocation::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $piveConv = new PiveConvocation();
        $piveConv->event_type = $request->event_type;
        $piveConv->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$piveConv
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>PiveConvocation::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  PiveConvocation  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,PiveConvocation $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
