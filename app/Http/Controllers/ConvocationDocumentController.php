<?php

namespace App\Http\Controllers;

use App\Models\ConvocationDocument;
use App\Models\Document;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ConvocationDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ConvocationDocument::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ConvocationDocument  $convocationDocument
     * @return \Illuminate\Http\Response
     */
    public function show(ConvocationDocument $convocationDocument)
    {
        //
    }

    public function search($id, $type)
    {
        $convocations = ConvocationDocument::where("id_convocation", "=", $id)->get();
        $data = [];

        foreach ($convocations as $conv)
        {   
            $doc = Document::find($conv->id_documents);
            $path = Storage::url($doc->path);
            if ($doc->type == $type){
                $data[] = $doc;
            } 
        }
    }

    public function SearchByConvocation($id)
    {
        $convocations = ConvocationDocument::where("id_convocation", "=", $id)->get();
        $data = [];

        foreach ($convocations as $conv)
        {   
            $doc = Document::find($conv->id_documents);
            $path = Storage::url($doc->path);
            $type = DocumentType::find($doc->type);
            $data[] = [
                'id' => $doc->id,
                'path' => $path,
                'description' => $doc->description,
                'type' => [
                    'id' => $type->id,
                    'description' => $type->description
                ]
            ];
        }

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $data
        ]);

    }

    public function SearchByConvocation2(Request $request)
    {
        $id = $request->id_convocation;
        $convocations = ConvocationDocument::where("id_convocation", "=", $id)->get();
        //print(sizeof($convocations));
        $data = [];

        if(sizeof($convocations) == 0)
        {
            return response()->json([
                'code' => 403,
                'msg'=> 'convocation does not exist',
                'data'=> []
            ]);    
        }
        
        
        foreach ($convocations as $conv)
        {   
            $doc = Document::find($conv->id_documents);
            $path = Storage::url($doc->path);
            $type = DocumentType::find($doc->type);
            $data[] = [
                'id' => $doc->id,
                'path' => $path,
                'description' => $doc->description,
                'type' => [
                    'id' => $type->id,
                    'description' => $type->description
                ]
            ];
        }

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $data
        ]);

    }

    public function image($id)
    {
        $document = Document::find($id);
        return response()->download(public_path(Storage::url($document->path))); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ConvocationDocument  $convocationDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(ConvocationDocument $convocationDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ConvocationDocument  $convocationDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ConvocationDocument $convocationDocument)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ConvocationDocument  $convocationDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(ConvocationDocument $convocationDocument)
    {
        //
    }
}
