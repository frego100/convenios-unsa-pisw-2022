<?php

namespace App\Http\Controllers;

use App\Models\LinkType;
use Illuminate\Http\Request;

class LinkTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>LinkType::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'category' => 'required'
        ]);
        
        $current_user = "admin";
        
        $link_type = new LinkType();
        $link_type->name = $request->name;
        $link_type->category = $request->category;
        $link_type->log_user_created = $current_user;
        $link_type->log_user_modified = $current_user;
        $link_type->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $link_type
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LinkType  $linkType
     * @return \Illuminate\Http\Response
     */
    public function show(LinkType $linkType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LinkType  $linkType
     * @return \Illuminate\Http\Response
     */
    public function edit(LinkType $linkType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LinkType  $linkType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LinkType $id)
    {
        //
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'successfull',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LinkType  $linkType
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinkType $linkType)
    {
        //
    }
}
