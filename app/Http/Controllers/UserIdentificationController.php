<?php

namespace App\Http\Controllers;

use App\Models\UserIdentification;
use Illuminate\Http\Request;

class UserIdentificationController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>UserIdentification::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userIdentification = new UserIdentification();
        $userIdentification->value = $request->value;
        $userIdentification->type = $request->type;
        $userIdentification->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$userIdentification
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>UserIdentification::findOrFail($id)
        ]);               
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  UserIdentification  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,UserIdentification $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
