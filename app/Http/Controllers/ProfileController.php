<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileResource;
use App\Models\Profile;
use App\Models\UserIdentification;
use Illuminate\Http\Request;
use Psy\Readline\Hoa\Console;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ProfileResource::collection(Profile::all()),
            
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $profile = new Profile();
        $profile->user_id=$request->user_id;
        if($request->hasFile("image")){
           
            //****$path =$request->image->store('photoProfiles','public');
            $path = "images_profiles";

            //***$path=Storage::url($path);
            $path=Storage::disk('s3')->put($path,$request->image,'public');
            $profile->image = $path;
        }

        $profile->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$profile
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>[
                'profile'=>new ProfileResource(Profile::findOrFail($id))]
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Profile  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $profile = Profile::find($id);
        $profile->name = $request->name;
        $profile->last_name = $request->last_name;
        $profile->address = $request->address;
        $profile->type=$request->type;
        $profile->phone = $request->phone;
        
        // $filename=$request->image->store('photoProfiles','public');
        //$profile->identification = $request->identification;
        $profile->birthdate = $request->birthdate;

        $id=UserIdentification::find($id);
        $id->type=$request->identification["type"];
        $id->value=$request->identification["value"];

        $profile->profile_created=$request->profile_created;
        
        $id->update();
        $profile->update();
        
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$profile,
            'identification'=>$id
            ]);

    }

    public function img(Request $request,$id)
    {
        $profile = Profile::find($id);
        
        if($request->hasFile('image')){
            //$url = str_replace('storage','public',$profile->image);
            Storage::disk('s3')->delete($profile->image);
            //****Storage::delete($url);

            $path = "images_profiles";
            //***$path=Storage::url($path);
            $path=Storage::disk('s3')->put($path,$request->image,'public');
            $profile->image = $path;

            //$path =$request->image->store('photoProfiles','public');
            //$path=Storage::url($path);
            //$profile->image=$path;


            $profile->update();

            return response()->json([
                'code'=>200,
                'msg'=>'exitoso img',
                'data'=>$profile,
                ]);  
        }else{

            return response()->json([
                'wa'=>'no entro'
            ]);
        }
              
    }

}
