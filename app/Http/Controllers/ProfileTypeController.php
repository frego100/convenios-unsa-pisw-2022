<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileTypeResource;
use App\Models\ProfileType;
use Illuminate\Http\Request;

class ProfileTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ProfileTypeResource::collection(ProfileType::all())]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profileType = new ProfileType();
        $profileType->description = $request->description;
        $profileType->save();

        return response()->json([
            'code'=>201,
            'msg'=>'ok',
            'data'=>$profileType
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>new ProfileTypeResource(ProfileType::findOrFail($id))
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ProfileType  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ProfileType $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }

}
