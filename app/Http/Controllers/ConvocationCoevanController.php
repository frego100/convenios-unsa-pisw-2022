<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConvocationCoevanResource;
use App\Http\Resources\ConvocationRequirementResource;
use App\Http\Resources\DocumentCoevanResource;
use App\Http\Resources\LinkResource;
use App\Http\Resources\RequirementResource;
use App\Models\Convocation;
use App\Models\ConvocationCoevan;
use App\Models\Document;
use App\Models\DocumentCoevan;
use App\Models\Link;
use App\Models\Requirement;
use Exception;
use Facade\Ignition\DumpRecorder\Dump;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Cast\Object_;

class ConvocationCoevanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>ConvocationCoevan::all()]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        //logger($request->all());
        
        $convCoevan = new ConvocationCoevan();        
        $convCoevan->id_convocation = $request->id_convocation;
        $convCoevan->id_academic_network = $request->id_academic_network;
        $convCoevan->id_university = $request->id_university;
        $convCoevan->avaltext = $request->avaltext;
        $convCoevan->coursestext = $request->coursestext;
        $convCoevan->commitment = $request->commitment;
        $convCoevan->semester = $request->semester;

        $docum =$request->documents;
        $link =$request->links;
        $files = $request->allFiles();
        $fil= $files['files'];
        //logger(get_class($fil[0]));

        for($i=0;$i< sizeof($docum);$i++){
            $d = json_decode($docum[$i],true);
            $doc = new DocumentCoevan();
            $doc->id_convocation = $request->id_convocation;
            $doc->name = $d['name'];
            $doc->type = $d['type'];

            $path = "filesCoevan";
            $path=Storage::disk('s3')->put($path,$fil[$i],'public');
            $doc->url = $path;

            $doc->description = $d['description'];
            $doc->save();
        }
        
        $list = array();
        
        foreach($link as $l){
            $l = json_decode($l,true);
            $link = new Link();
            $link->id_convocation = $request->id_convocation;
            $link->type = $l["type"];
            $link->name = $l["name"];
            $link->url = $l["url"];
            $link->description = $l['description'];

            $list[] =array('name' =>$l["name"],'type' => $l["type"], 'url' =>$l['url'],'description'=>$l['description'] );
            $link->save();
        }

        $conv = Convocation::find($request->id_convocation);
        $requeriments =$request->requirements;
        $conv->requirements()->attach($requeriments);
        $convCoevan->save();
        $idConvCoevan= $convCoevan->id;
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=> [
                new ConvocationCoevanResource(ConvocationCoevan::findOrFail($idConvCoevan)),
                'links' => json_encode($list),
                //'links' => LinkResource::collection(Link::all),
                'requirements' =>RequirementResource::collection(Requirement::findOrFail($requeriments)),
                'documents' => DocumentCoevanResource::collection(DocumentCoevan::where('id_convocation',$request->id_convocation)->get())
                ]            
        ]);       
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            //'data'=> ConvocationCoevan::findOrFail($id)
            'data' => new ConvocationCoevanResource(ConvocationCoevan::findOrFail($id))
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  ConvocationCoevan  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ConvocationCoevan $id)
    {
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'exitoso',
            'data'=>$id
            ]);
    }
}
