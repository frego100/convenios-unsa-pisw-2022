<?php

namespace App\Http\Controllers;

use App\Models\AcademicNetwork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AcademicNetworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academic_networks = AcademicNetwork::where("log_status", "=", "0")->get();
        
        return response()->json([
            'code'=>200,
            'msg'=>'ok',
            'data'=>$academic_networks
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'acronym' => 'required',
            'description' => 'required',
            'logo' => 'required'
        ]);
        
        $current_user = "admin";
        
        $academic_network = new AcademicNetwork();
        $academic_network->name = $request->name;
        $academic_network->acronym = $request->acronym;
        $academic_network->description = $request->description;

        // guardar el logo de la red academica        
        // $academic_network->logo = $request->logo;
        //GUARDAR LOGO EN S3
        $path = "logoAcademicNetwork";
        $path=Storage::disk('s3')->put($path,$request->logo,'public');
        $academic_network->logo = $path;


        $academic_network->log_user_created = $current_user;
        $academic_network->log_user_modified = $current_user;
        $academic_network->save();

        return response()->json([
            'code' => 200,
            'msg'=> 'ok',
            'data'=> $academic_network
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AcademicNetwork  $academicNetwork
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $academic_network=AcademicNetwork::find($id);
        
        if($academic_network != null){
            return response()->json([
                'code' => 200,
                'msg' => 'ok',
                'data'=> $academic_network 
            ]);
        } else {
            return response()->json([
                'code' => 404,
                'msg' => 'Academic Network not found',
                'data'=> ''
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AcademicNetwork  $academicNetwork
     * @return \Illuminate\Http\Response
     */
    public function edit(AcademicNetwork $academicNetwork)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AcademicNetwork  $academicNetwork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AcademicNetwork $id)
    {
        //
        $id->update($request->all());
        return response()->json([
            'code'=>200,
            'msg'=>'successfull',
            'data'=>$id
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AcademicNetwork  $academicNetwork
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $academic_network = AcademicNetwork::find($id);
        if($academic_network) {
            $academic_network->log_status = 1;
            $academic_network->save();

            return response()->json([
                'code'=>200,
                'msg'=>'eliminado',
                'data'=>$academic_network
            ]);
        }
    }
}
