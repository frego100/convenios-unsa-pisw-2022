<?php

namespace App\Http\Resources;

use App\Models\AcademicYear;
use App\Models\CurrentCycle;
use App\Models\Faculty;
use App\Models\PostulationCoevanCourse;
use App\Models\PostulationCoevanState;
use App\Models\Program;
use App\Models\University;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PostulationCoevanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $postulationCoevan = $this;

        $university = University::find($postulationCoevan->origin_university);
        $faculty = Faculty::find($postulationCoevan->id_faculty);
        $profesionalProgram = Program::find($postulationCoevan->id_professional_program);
        $currenCicle = CurrentCycle::find($postulationCoevan->id_current_cicle);
        $academicYear = AcademicYear::find($postulationCoevan->id_academic_year);

        $postState = PostulationCoevanState::find($postulationCoevan->post_state);
        $courses = PostulationCoevanCourse::where('id_postulation',$postulationCoevan->id)->get();
        $c = array();
        foreach ($courses as $co){
            $c[] = array('id' => $co['id'],'number_credits' => $co['number_credits'],'course_code' => $co['course_code'], 'unsa_course_name'=>$co['unsa_course_name'], 'year' => $co['year'], 'semester'=> $co['semester'], 'target_university_course_name' => $co['target_university_course_name']);

        }

        $files = Storage::disk('s3') -> url($this->postulation_document);
        $photo = Storage::disk('s3') -> url($this->photo);
        return [
            
            'id'=>$this->id,
            'id_convocation' => $postulationCoevan->id_convocation,
            'id_user' => $postulationCoevan->id_user,
            'lastname' => $postulationCoevan->lastname,
            'name' => $postulationCoevan->name,
            'birth_date' => $postulationCoevan->birth_date,
            'dni' => $postulationCoevan->dni,
            'city_region_postulant' => $postulationCoevan->city_region_postulant,
            'cui' => $postulationCoevan->cui,
            'current_address' => $postulationCoevan->current_address,
            'phone' => $postulationCoevan->phone,
            'email' => $postulationCoevan->email,
            'photo' => $photo,
            'contact_emergency_number' => $postulationCoevan->contact_emergency_number,

            'origin_university' => [
                'id' => $university->id,
                'name' => $university->name,
                'acronym' => $university->acronym
            ],
            'web_page' => $postulationCoevan->web_page,
            'city_region_university' => $postulationCoevan->city_region_university,
            'faculty' => [
                'id' => $faculty->id,
                'name' => $faculty->name,
                'acronym' => $faculty->acronym
            ],
            'professional_program' =>[
                'id' => $profesionalProgram->id,
                'name' => $profesionalProgram->name,
                'acronym' => $profesionalProgram->acronym,
                'faculty' => $profesionalProgram->faculty

            ],
            'current_cicle' =>[
                'id' => $currenCicle->id,
                'description' => $currenCicle->description
            ],
            'academic_year' => [
                'id' => $academicYear->id,
                'description' => $academicYear->description
            ],
            'mean_grades' => $postulationCoevan->mean_grades,
            'total_credits' => $postulationCoevan->total_credits,
            'coordinator' => $postulationCoevan->coordinator,
            'coordinator_cargue' => $postulationCoevan->coordinator_cargue,
            'coordinator_email' => $postulationCoevan->coordinator_email,

            'postulation_document' => $files,

            'courses' => $c,
            'post_state' => [
                'id' => $postState->id,
                'name' => $postState ->name,
                'description' => $postState->description
            ],

        ];        
    }
}
