<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AcademicNetworkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $logo =null;
        if($this->logo!=null)
            $logo = Storage::disk('s3') -> url($this->logo);

        return [
            'id'=>$this->id,
            'name' => $this->name,
            'acronym'=> $this->acronym,
            'description' => $this->description,
            'logo' => $logo
        ];
    }
}
