<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserIdentificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'value'=>$this->value,
            //'type'=>$this->type,
           /* 'type' => [
                'id'=>$this->userIdentificationType->id,
                'description' => $this->userIdentificationType->description
            ]*/
            'type'=>new UserIdentificationTypeResource($this->userIdentificationType)
        ];
    }
}
