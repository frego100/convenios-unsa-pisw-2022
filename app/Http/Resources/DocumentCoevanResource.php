<?php

namespace App\Http\Resources;

use App\Models\AcademicNetwork;
use App\Models\University;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class DocumentCoevanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $files = Storage::disk('s3') -> url($this->url);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'url' => $files,
            'description' => $this->description

        ];
    }
}
