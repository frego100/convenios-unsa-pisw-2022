<?php

namespace App\Http\Resources;

use App\Models\ConvocationState;
use App\Models\ConvocationType;
use App\Models\Modality;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ConvocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $type = ConvocationType::find($this->type);
        $modality = Modality::find($this->modality);
        $state = ConvocationState::find($this->conv_state);
        $afiche = null;
        if($this->afiche!=null){
            $afiche= Storage::disk('s3') -> url($this->afiche);
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'correlative' => $this->correlative,
            'type' => [
                'id' => $type->id,
                'name' => $type->name,
                'acronym' => $type->acronym
            ],
            'modality' => [
                'id' => $modality->id,
                'name' => $modality->name
            ],
            'description' => $this->description,
            'conv_state' => [
                'id' => $state->id,
                'name' => $state->name,
                'description' => $state->description
            ],
            'important_notes' => $this->important_notes,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'afiche' => $afiche
        ];
    }
}
