<?php

namespace App\Http\Resources;

use App\Models\AcademicNetwork;
use App\Models\DocumentCoevan;
use App\Models\Link;
use App\Models\LinkType;
use App\Models\Requirement;
use App\Models\University;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ConvocationCoevanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {       
        $links = Link::where('id_convocation',$this->id_convocation)->get();
        $documents = DocumentCoevan::where('id_convocation',$this->id_convocation)->get();

        $c = array();
        $d = array();
        $r = array();
        foreach ($links as $co){
            $linkT = LinkType::find($co->type);
            $c[] = array('id' => $co['id'], 'name' => $co['name'],'type' => ['id'=>$linkT->id,'name'=>$linkT->name,'category'=>$linkT->category],'url' => $co['url'], 'description'=>$co['description']);
        }        


        foreach ($documents as $co){
            $doc = Storage::disk('s3') -> url($co['url']);
            $linkT = LinkType::find($co->type);
            $d[] = array('id' => $co['id'], 'name' => $co['name'], 'type' => ['id'=>$linkT->id,'name'=>$linkT->name,'category'=>$linkT->category], 'url' =>$doc, 'description'=>$co['description']);
        }

        $req_conv = DB::table('convocation_requirement')->where('convocation_id',$this->id_convocation)->get();

        foreach ($req_conv as $re){
            $docT = Requirement::find($re->requirement_id);
            //logger($docT);
            $r[] = array('id' => $docT['id'], 'description' => $docT['description']);
        }


        return [
            'id'=>$this->id,
            'academicNetwork'=> new AcademicNetworkResource($this->academicNetwork),
            'university'=>new UniversityResource($this->university),
            'links' => $c,
            'documents' => $d,
            'requirements' => $r,
            'avaltext' => $this->avaltext,
            'coursestext' => $this->coursestext,
            'commitment' => $this->commitment,
            'semester' => $this->semester
        ];
    }
}
