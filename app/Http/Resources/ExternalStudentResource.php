<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ExternalStudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $justification = null;
        if($this->justification!=null)
            $justification = Storage::disk('s3') -> url($this->justification);

        return [
            'id'=>$this->id,
            'name' => $this->name,
            'lastname'=> $this->lastname,
            'justification' => $justification,
            'status_request' => $this->status_request,
            'email' => $this->email,
            'password_created' => $this->password_created
        ];
    }
}
