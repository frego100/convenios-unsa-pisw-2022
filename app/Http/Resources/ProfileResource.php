<?php

namespace App\Http\Resources;

use App\Models\ProfileType;
use App\Models\UserIdentification;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       
        //$url = 'https://s3.'.env('AWS_DEFAULT_REGION').'.amazonaws.com/'.env('AWS_BUCKET').'/';
        $files = Storage::disk('s3') -> url($this->image);
        //$files = preg_replace("\/\//",'/',$files);
        
        return [
            
            'id'=>$this->id,
            //'image'  => (preg_match('.google.',$this->image))? $this->image: asset( $this->image),
            'image'  => (preg_match('.google.',$this->image))? $this->image:$files,
            //'image'=>$this->image,
            'name'=>$this->name,
            'last_name'=>$this->last_name,
            'address'=>$this->address,
            //'type'=> ProfileTypeResource::collection($this->whenLoaded('type')),
            //'type'=>ProfileType::find($this->type),
            'type'=>new ProfileTypeResource($this->profileType),
            'phone'=>$this->phone,
            'identification'=> new UserIdentificationResource($this->userIdentification),
            //'identification'=> new UserIdentificationResource($this->whenLoaded('user_identification_types',$this->userIdentification)),
            'birthdate'=>$this->birthdate,
            'profile_created'=>$this->profile_created
        ];

    }
}
