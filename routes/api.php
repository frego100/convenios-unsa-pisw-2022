<?php

use App\Http\Controllers\AcademicNetworkController;
use App\Http\Controllers\AcademicYearController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminRolesController;
use App\Http\Controllers\ConvocationCoevanController;
use App\Http\Controllers\ConvocationController;
use App\Http\Controllers\ConvocationDocumentController;
use App\Http\Controllers\ConvocationStateController;
use App\Http\Controllers\ConvocationTypeController;
use App\Http\Controllers\CurrentCycleController;
use App\Http\Controllers\DocumentCoevanController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DocumentTypeController;
use App\Http\Controllers\EventTypeController;
use App\Http\Controllers\ImagenController;
use App\Http\Controllers\ExternalStudentController;
use App\Http\Controllers\FacultyController;
use App\Http\Controllers\LinkTypeController;
use App\Http\Controllers\ModalityController;
use App\Http\Controllers\PivConvocationController;
use App\Http\Controllers\PivdoConvocationController;
use App\Http\Controllers\PiveConvocationController;
use App\Http\Controllers\PostulationCoevanController;
use App\Http\Controllers\PostulationCoevanStateController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProfileTypeController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\RequirementController;
use App\Http\Controllers\UniversityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserIdentificationController;
use App\Http\Controllers\UserIdentificationTypeController;
use App\Models\AcademicNetwork;
use App\Models\CurrentCycle;
use App\Models\ExternalStudent;
use App\Models\University;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users/{id}',[UserController::class,'show']);
Route::post('/register',[UserController::class,'store']);
Route::post('/login',[UserController::class,'login']);
Route::post('/login2',[UserController::class,'externalStudentLogin']);

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => ["auth:sanctum"]],function(){
    Route::post('/logout',[UserController::class,'logout']);
    Route::post('admins/logout', [AdminController::class, 'logout']);
});

//users
Route::get('/users','App\Http\Controllers\UserController@index');
Route::post('/users','App\Http\Controllers\UserController@store');
Route::get('/users/{id}','App\Http\Controllers\UserController@show');

//profileTypes
Route::get('profileTypes',[ProfileTypeController::class,'index']);
Route::post('profileTypes',[ProfileTypeController::class,'store']);
Route::put('profileTypes/{id}',[ProfileTypeController::class,'update']);

//convocationTypes
Route::get('get-all-convocation-types',[ConvocationTypeController::class,'index']);
Route::post('convocationTypes',[ConvocationTypeController::class,'store']);
Route::put('convocationTypes/{id}',[ConvocationTypeController::class,'update']);

//eventTypes
Route::get('eventTypes',[EventTypeController::class,'index']);
Route::post('eventTypes',[EventTypeController::class,'store']);
Route::put('eventTypes/{id}',[EventTypeController::class,'update']);

//requirements
Route::get('requirements',[RequirementController::class,'index']);
Route::post('requirements',[RequirementController::class,'store']);
Route::put('requirements/{id}',[RequirementController::class,'update']);

//userIdentificactionTypes
Route::get('userIdentificationTypes',[UserIdentificationTypeController::class,'index']);
Route::post('userIdentificationTypes',[UserIdentificationTypeController::class,'store']);
Route::put('userIdentificationTypes/{id}',[UserIdentificationTypeController::class,'update']);

//userIdentifications
Route::get('userIdentifications',[UserIdentificationController::class,'index']);
Route::post('userIdentifications',[UserIdentificationController::class,'store']);
Route::put('userIdentifications/{id}',[UserIdentificationController::class,'update']);

//profiles
Route::get('profiles',[ProfileController::class,'index']);
Route::get('profiles/{id}',[ProfileController::class,'show']);
Route::post('profiles',[ProfileController::class,'store']);
Route::put('profiles/{id}',[ProfileController::class,'update']);
Route::post('imagen/{id}',[ProfileController::class,'img']);

//pivConvocations
Route::get('pivConvocations',[PivConvocationController::class,'index']);
Route::post('pivConvocations',[PivConvocationController::class,'store']);
Route::put('pivConvocations/{id}',[PivConvocationController::class,'update']);

//convocationCoevans
Route::post('create-convocation-coevan',[ConvocationCoevanController::class,'store']);
Route::get('convocation-coevan-all',[ConvocationCoevanController::class,'index']);
Route::get('get-convocation-coevan-detail',[ConvocationCoevanController::class,'show']);

Route::get('documentCoevan',[DocumentCoevanController::class,'index']);

//postulationCoevanState
Route::get('postulationCoevanState',[PostulationCoevanStateController::class,'index']);
Route::post('postulationCoevanState',[PostulationCoevanStateController::class,'store']);
Route::put('postulationCoevanState/{id}',[PostulationCoevanStateController::class,'update']);


Route::post('create-postulation-coevan',[PostulationCoevanController::class,'store']);
Route::get('postulation',[PostulationCoevanController::class,'show']);
Route::get('postulation-coevan-convocation-user',[PostulationCoevanController::class,'showPostulationByConvocationUser']);
Route::get('postulation-by-convocation',[PostulationCoevanController::class,'showPostulationByConvocation']);


/*
Route::get('pivdoConvocations',[PivdoConvocationController::class,'index']);
Route::post('pivdoConvocations',[PivdoConvocationController::class,'store']);
Route::put('pivdoConvocations/{id}',[PivdoConvocationController::class,'update']);

Route::get('piveConvocations',[PiveConvocationController::class,'index']);
Route::post('piveConvocations',[PiveConvocationController::class,'store']);
Route::put('piveConvocations/{id}',[PiveConvocationController::class,'update']);
*/


//Route::apiResource('pivdoConvocations',PivdoConvocationController::class)->only(['index','store','update']);
//Route::apiResource('piveConvocations',PiveConvocationController::class)->only(['index','store','update']);;

// docyment_types table
Route::post('documentTypes',[DocumentTypeController::class,'store']);
Route::get('documentTypes/{id}',[DocumentTypeController::class,'show']);
Route::put('documentTypes/{id}',[DocumentTypeController::class,'update']);
Route::delete('documentTypes/{id}',[DocumentTypeController::class,'destroy']);
Route::get('documentTypes',[DocumentTypeController::class,'index']);

// document table
Route::post('documents',[DocumentController::class,'store']);
Route::get('documents/{id}',[DocumentController::class,'show']);
Route::put('documents/{id}',[DocumentController::class,'update']);
Route::delete('documents/{id}',[DocumentController::class,'destroy']);
Route::get('documents',[DocumentController::class,'index']);

// convocation table
Route::post('create-convocation',[ConvocationController::class,'store']);
Route::get('convocations',[ConvocationController::class,'show']);
//Route::put('convocations/{id}',[ConvocationController::class,'update']);
Route::put('convocation-change-state',[ConvocationController::class,'changeState']);
Route::delete('convocations/{id}',[ConvocationController::class,'destroy']);
Route::get('convocations/all',[ConvocationController::class,'index']);

// convocation document table
Route::get('convocation-documents',[ConvocationDocumentController::class,'SearchByConvocation2']);
Route::get('convocation-documents/all',[ConvocationDocumentController::class,'index']);
Route::get('convocation-documents/{id}',[ConvocationController::class,'show']);

// admin table
Route::post('admins/register', [AdminController::class, 'store']);
Route::post('admins/login', [AdminController::class, 'login']);
Route::get('admins/all',[AdminController::class, 'index']);
Route::get('admins',[AdminController::class, 'show']);
Route::put('admins/{id}',[AdminController::class,'update']);
Route::delete('admins/{id}',[AdminController::class,'destroy']);

// admin roles table
Route::post('admin-roles',[AdminRolesController::class, 'store']);
Route::get('admin-roles/all',[AdminRolesController::class, 'index']);
Route::put('admin-roles/{id}',[AdminRolesController::class,'update']);

// academic networks table
Route::post('academic-networks', [AcademicNetworkController::class, 'store']);
Route::get('get-all-academic-networks', [AcademicNetworkController::class, 'index']);
Route::get('academic-networks/{id}', [AcademicNetworkController::class, 'show']);
Route::put('academic-networks/{id}', [AcademicNetworkController::class, 'update']);
Route::delete('academic-networks/{id}', [AcademicNetworkController::class, 'destroy']);


// universities table
Route::post('universities', [UniversityController::class, 'store']);
Route::post('assign-academic-network', [UniversityController::class, 'assignAcademicNetwork']);
Route::get('get-all-universities', [UniversityController::class, 'index']);
Route::get('get-all-assigns', [UniversityController::class, 'listAllAssigns']);
Route::get('universities/{id}', [UniversityController::class, 'show']);
Route::get('get-universities-by-academic-network', [UniversityController::class, 'getByAcademicNetwork']);
Route::delete('universities/{id}',[UniversityController::class,'destroy']);
Route::put('universities/{id}', [UniversityController::class, 'update']);

// link types table
Route::post('link-types', [LinkTypeController::class, 'store']);
Route::get('get-all-link-types', [LinkTypeController::class, 'index']);
Route::put('link-types/{id}', [LinkTypeController::class, 'update']);

// faculties table
Route::get('get-all-faculties', [FacultyController::class, 'index']);
Route::delete('faculties/{id}',[FacultyController::class,'destroy']);

// programs table
Route::get('get-all-programs', [ProgramController::class, 'index']);
Route::delete('programs/{id}',[ProgramController::class,'destroy']);

// academic years table
Route::get('get-all-academic-years', [AcademicYearController::class, 'index']);

// current cycles table
Route::get('get-all-cycles', [CurrentCycleController::class, 'index']);

// modalities table
Route::get('get-all-modalities',[ModalityController::class, 'index']);
Route::delete('modalities/{id}',[ModalityController::class,'destroy']);

//external students table
Route::get('external-students/all',[ExternalStudentController::class, 'index']);
Route::get('external-students/remaining',[ExternalStudentController::class, 'remainingRequests']);
Route::get('external-students/{id}',[ExternalStudentController::class, 'show']);
Route::post('external-students/create',[ExternalStudentController::class, 'store']);
Route::put('external-students',[ExternalStudentController::class, 'update']);
Route::put('external-students/reject',[ExternalStudentController::class, 'reject']);
Route::delete('external-students/{id}',[ExternalStudentController::class, 'destroy']);

// Convocation state table
Route::post('convocation-states', [ConvocationStateController::class, 'store']);
Route::get('convocation-states/all',[ConvocationStateController::class, 'index']);
Route::get('convocation-states/{id}',[ConvocationStateController::class, 'show']);
